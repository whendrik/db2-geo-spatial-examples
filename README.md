# Db2 Geo Spatial Examples

- [Official Documentation - Db2 Spatial Extender](https://www.ibm.com/support/producthub/db2/docs/content/SSEPGG_11.5.0/com.ibm.db2.luw.spatial.topics.doc/doc/csbp1001.html)
- [Tutorial GeoSpatial Extension](https://www.ibm.com/developerworks/data/tutorials/dm-1202db2spatialdata1/index.html) and [Part II](https://www.ibm.com/developerworks/data/tutorials/dm-1212db2spatialdata2/index.html)
- There is support for different [Reference Systems](https://en.wikipedia.org/wiki/Spatial_reference_system)

## Why you want to use Db2 Spatial Extenion?

* Support for `ST_Point()`, `ST_Polygon()` and use functions like `ST_Distance()` and `ST_Within()`
* Indexes on Spatial Data for faster queries
* Able to load shapefiles and other common geo formats.

---

## Enable the Spatial Extension with `db2se` (CP4D)

1. `oc rsh` to the node lands as `db2uadm`, and `su - db2inst1` to change UID
2. Check indeed it is disabled by `db2se disable_db BLUDB -force 1` 
3. Enable with `db2se enable_db BLUDB` (takes <5 minutes)

![install](https://gitlab.com/whendrik/db2-geo-spatial-examples/-/raw/master/images/enable_db2gse.png)